import { createApp } from 'vue'
import App from './App.vue'
import router from "./router";
import {store} from "./store";
import OpenLayersMap from "vue3-openlayers";
import VueDatePicker from '@vuepic/vue-datepicker';
import './style.css'
import '@vuepic/vue-datepicker/dist/main.css'
import "vue3-openlayers/styles.css";
import 'vue-multiselect/dist/vue-multiselect.css'
// import Multiselect from 'vue-multiselect'

const mapOptions = {debug: false}
const app = createApp(App)

app.use(router)
app.use(store)
app.use(OpenLayersMap, mapOptions)
// app.use(Multiselect)
app.component('VueDatePicker', VueDatePicker);
app.mount('#app')