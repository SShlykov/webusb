import {typeCommands, bufferSize, getViewType, bufferType} from '../helpers';
import moment from 'moment';
moment.locale('ru');

const state = {
    device: null,
    deviceLogs: [],
    answerSetting: {},
    answersSettings: [],
    answersValues: [],
    answersRead: [],
    result: {},
    closeConnection: false,
    settings: []
};
const getters= {
    device:     (state) => state.device,
    deviceLogs: (state) => state.deviceLogs,
    deviceAnswer: (state) => state.answerSetting,
    answersSettings: (state) => state.answersSettings,
    answersValues: (state) => state.answersValues,
    deviceAnswersRead: (state) => state.answersRead,
    closeConnection: (state) => state.closeConnection,
    deviceStatus: (state) => !!state.device && JSON.stringify(state.device.opened),
    settings: (state) => state.settings,
}

const mutations = {
    setDevice:  (state, updVal) => state.device = updVal,
    putLog:     (state, newLog) =>  {
        // console.log(newLog)
        if (newLog.text && newLog.dt) {
            state.deviceLogs.push(newLog)
        }
    },
    putAnswerSetting: (state, newAnswer) => {
        if (newAnswer.id && newAnswer.dt) {
            state.answersSettings.push(newAnswer)
        }
    },
    putAnswerValue: (state, newAnswer) => {
        if (newAnswer.id && newAnswer.dt) {
            state.answersValues.push(newAnswer)
        }
    },
    putAnswerRead: (state, newAnswer) => {
        if (newAnswer.text && newAnswer.dt) {
            state.answersRead.push(newAnswer)
        }
    },
    answerSetting: (state, updVal) => state.answerSetting = updVal,
    setSettings: (state, updVal) => state.settings = updVal,
    clearState: (state, key) => state[key] = [],
    setResult: (state, updVal) => state.result = updVal,
    closeConnection: (state, updVal) => state.closeConnection = updVal
};

const actions = {
    connectDevice: async ({commit, dispatch, state}) =>  {
        if (!navigator.usb) {
            console.error('WebUSB not supported by this browser.')
            return
        }
        try {
            state.device = await navigator.usb.requestDevice({filters: [{}]});
            // console.log('state.device', state.device)
            if (state.device) {
                await state.device.open()
                await state.device.selectConfiguration(1)
                await state.device.claimInterface(0)
                commit('clearState', 'deviceLogs')
                commit('clearState', 'answersSettings')
                commit('clearState', 'answersValues')
                commit('clearState', 'answersRead')
                commit('clearState', 'answerSetting')
                commit('closeConnection', false)
                commit('putLog', "Device connected.")
                dispatch('readLogs')
                commit('setDevice', state.device)
                return {device: state.device}
            }
            // commit('setDevice', state.device)
        } catch (err) {
            commit('setDevice', null)
            console.error('Error connecting to device:', err);
        }
    },
    disConnectDevice: async ({commit, state}) => {
        if (!state.device) {
            console.error('WebUSB not connected by this browser.')
            return
        }
        try {
            commit('putLog', "Device disconnected.")
            commit('closeConnection', true)
            commit('setResult', {})
            commit('clearState', 'deviceLogs')
            await state.device.close()
            commit('setDevice', null)
        } catch (err) {
            console.error('Error disconnecting from device:', err)
        }
    },
    readLogs: async ({commit, getters, dispatch, state}) => {
        try {
            while (!state.closeConnection && state.device && state.device.opened) {
                const getCloseConnection = getters.closeConnection
                await state.device.transferIn(3, 64).then(result => {
                    commit('setResult', result)
                    const decoder = new TextDecoder()
                    const message = decoder.decode(result.data)
                    message.slice(0, -1)
                    const item = {
                        text: message,
                        dt: moment().format("DD.MM.YYYY hh:mm:ss")
                    }
                    commit('putLog', item)
                    if(getCloseConnection && result.status === 'ok') return
                })
                .catch(error => {
                    // console.error('Error reading from device:', error)
                    console.log('not connected to device')
                    return
                })
                if(getCloseConnection) break
            }
        } catch (err) {
            console.error("Read error:", err)
        }
    },
    putCommands: async ({commit, getters, dispatch, state}, {device = null, typeData, command}) => {
    try {
            if(!command) return
            const arrComs = command.split(',')
            device = device ? device : getters.device
            const typeCommand = arrComs[0]
            // console.log('typeCommand', typeCommand)
            // console.log('typeData', typeData)
            // console.log('command', command)

            const buffer = new ArrayBuffer(bufferSize(arrComs[2], typeCommand, typeData))
            const viewData = getViewType(buffer, arrComs, typeCommand, typeData)
            // console.log('viewData', viewData)

            if(device) {
                await device.transferOut(1, viewData).then(result => {
                    commit('setResult', result)
                    const decoder = new TextDecoder()
                    const message = decoder.decode(result.data)
                    message.slice(0, -1)
                    const item = {
                        text: message,
                        dt: moment().format("DD.MM.YYYY hh:mm:ss")
                        }
                    commit('putLog', item)
                    if(result.status === 'ok') {
                        // console.log('result', result)
                        dispatch('readAnswers', {device: device, command: arrComs})
                    }
                })
                .catch(error => {
                    // console.error('Error reading from device:', error)
                    return console.log('not connected to device')
                })
        }
        } catch (err) {
            console.error("Read error:", err)
        }
    },
    readAnswers: async ({commit, getters, dispatch, state}, {device, command}) => {
        try {
            if (!device) return
            while (!state.closeConnection && state.device && state.device.opened) {
                const getCloseConnection = getters.closeConnection
                await device.transferIn(2, 64).then(result => {
                    // console.log('result', result)
                    const dataView = new DataView(result.data.buffer)
                    const typeCommand = dataView.getUint8(0, true).toString(16)
                    const idCommand = dataView.getUint8(1, true).toString(16)
                    const settings = getters.settings
                    const answersSettings = getters.answersSettings
                    const answersValues = getters.answersValues
                    const typeCommandConfig = typeCommands(typeCommand)
                    const currentTypeSettings = settings.filter((set) => set.type === typeCommandConfig)
                    const params = currentTypeSettings.map(item => item.params).flat()
                    const currentProxySet = params.find(item => item.id === Number(idCommand))
                    const currentSet = currentProxySet ? JSON.parse(JSON.stringify(currentProxySet)) : null
                    const dt = moment().format("DD.MM.YYYY hh:mm:ss")
                    if(!currentSet) return commit('putAnswerRead', {dt, text: 'Неправильный id'})
                    const typeData = currentSet.type
                    let arrView = bufferType(typeData, result.data.buffer)
                    let value32 = 0
                    if (typeData === 'uint32') {
                        /* 1 способ Read uint32 value from buffer */
                        // let newBuff = [...arrView]
                        // const newArr = arrView.subarray(0, 4)
                        // const newView = new Uint8Array(newBuff.slice(2))
                        /* Read uint32 value from buffer */
                        const dataView = new DataView(arrView.buffer)
                        value32 = dataView.getUint32(2, true)
                    }
                    arrView = [...arrView]
                    const isNumber = currentSet.type.includes('uint')
                    // console.log('currentSet', currentSet)
                    if(isNumber) {
                        const value = (typeData === 'uint32' && value32) ? value32 : arrView[0]
                        // const value = buffer.slice(2).reduce((acc, num) => {
                        //     if(num === 0) {
                        //         return acc
                        //     } else {
                        //     return acc += num
                        //     }
                        // }, 0)
                        const currentSetting = {id: idCommand, type: typeCommandConfig, key: currentSet.key, title: currentSet.title, value: value, dt: dt}
                        // const currentSetting = {id: idCommand, type: typeCommand, key: currentSet.key, title: currentSet.title, value: parseInt(value, 16), dt: dt}
                        const message = `${currentSet.title}: ${value}`
                        // console.log('currentSetting', currentSetting)

                        if(typeCommandConfig === 'settings') {
                            const doubleSet = answersSettings.find((el) => el.key === currentSetting.key)
                            if(!doubleSet) {
                                commit('putAnswerSetting', currentSetting)
                            }
                        }
                        if(typeCommandConfig === 'values') {
                            const doubleSet = answersValues.find((el) => el.key === currentSetting.key)
                            if(!doubleSet) commit('putAnswerValue', currentSetting)
                        }
                        commit('answerSetting', currentSetting)
                        commit('putAnswerRead', {dt, text: message})
                    }

                    if(!isNumber) {
                        const decoder = new TextDecoder()
                        const text = decoder.decode(result.data).slice(2)
                        const message = `${currentSet.title}: ${text}`
                        const textWithoutZero = text.slice(0, text.indexOf('\u0000'))
                        const currentSetting = {id: idCommand, type: typeCommandConfig, key: currentSet.key, title: currentSet.title, value: textWithoutZero, dt: dt}
                        if(typeCommandConfig === 'settings') {
                            const doubleSet = answersSettings.find((el) => el.key === currentSetting.key)
                            if(!doubleSet) commit('putAnswerSetting', currentSetting)
                        }
                        if(typeCommandConfig === 'values') {
                            const doubleSet = answersValues.find((el) => el.key === currentSetting.key)
                            if(!doubleSet) commit('putAnswerValue', currentSetting)
                        }
                        commit('answerSetting', currentSetting)
                        commit('putAnswerRead', {dt, text: message})
                    }

                    if(getCloseConnection && result.status === 'ok') return
                })
                .catch(error => {
                    // console.error('Error reading from device:', error)
                    console.log('not connected to device')
                    return
                })
            }
        } catch (err) {
                console.error("Read error:", err)
        }
    },
    setSettings: async ({commit}, settings) => {
        try {
            commit('setSettings',  settings)
        }
        catch (err) {
            console.error('Error getting settings:', err);
        }
    }
};

export const store = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};