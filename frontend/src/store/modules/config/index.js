import {initialError, initialObject, initialText, initialButtonState} from "../initState.js";
import {onButtonClick, loggerTextToNumber} from "../helpers.js";
import {saveFile} from "../apiHelpers.js";

const state = {
    config:         initialObject(),
    configError:    initialError(),
    configIsOpen:   false,
    configText:     initialText(),
    configButton:   initialButtonState(),
};
const getters= {
    config:         (state) => state.config,
    configError:    (state) => state.configError,
    configIsOpen:   (state) => state.configIsOpen,
    configText:     (state) => state.configText,
    configButton:   (state) => state.configButton,
};

const mutations = {
    setConfig:          (state, updVal) => state.config = updVal,
    setConfigError:     (state, updVal) => state.configError = updVal,
    toggleConfigIsOpen: (state) => state.configIsOpen = !state.configIsOpen,
    setConfigText:      (state, updVal) => state.configText = updVal,
    setConfigButton:    (state, updVal) => state.configButton = updVal,
};

const actions = {
    setConfig: ({commit}, template) => {
        commit('setConfig', template)
        commit('setConfigText', JSON.stringify(template, null, 2))
    },
    setConfigText: ({commit}, e) => {
        try {
            let textConfig = e.target.value;
            commit('setConfigText', textConfig)
            commit('setConfig', JSON.parse(textConfig))
            commit('setConfigError', null)
        } catch (e) {
            console.log(e)
            commit('setConfigError', e)
        }
    },
    handleSettingUpdate: ({commit, getters}, e) => {
        console.log('updateConfig', e)
        const {target} = e,
        {value, id} = target
        // const currentValue = id === 'log_level' ? loggerTextToNumber(value) : value,
        const new_config = {...getters.config, [id]: value};

        commit('setConfig', new_config)
        commit('setConfigText', JSON.stringify(new_config, null, 2))
    },
    updateConfig: ({commit, getters}, settings) => {
        console.log('updateConfig', settings.key)

            const new_config = {...getters.config, [settings.key]: settings.value};

        commit('setConfig', new_config)
        commit('setConfigText', JSON.stringify(new_config, null, 2))
    },
    onSaveConfig: async ({commit, getters}) => {
        onButtonClick(getters.configButton,
            () => saveFile("config.json", getters.configText, getters.configButton,
                () => commit('setConfigButton', initialButtonState())
            )
        )
    }
};

export const store = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};