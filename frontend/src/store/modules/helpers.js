import {initialButtonState} from "./initState.js";

let resetTimer = null;

const resetButtonClick = (btn) => {
    Object.assign(btn, initialButtonState());
}

const onButtonClick = (btn, sender_callback) => {
    sender_callback();
    if (btn.loading) return;
    if (btn.success) {
        if (resetTimer) clearTimeout(resetTimer);
        resetButtonClick(btn);
        return;
    }

    btn.loading = true;
    btn.disabled = true;
}

const getViewType = (buffer, command, typeCommand, typeData) => {

    // const buffer16 = new ArrayBuffer(6)

        /* 1 способ запись в Uint16Array
            // let int16 = new Uint8Array(buffer16)
            // int16[0] = [`0x21`]
            // int16[1] = [`0x27`]
            // int16 = new Uint16Array(buffer16)
            // int16[1] = ['0x160']
        */
        /* 2 способ запись в Uint16Array
            // int16 = new Uint16Array(buffer16)

            //  int16[0] = [`${0x21 | 0x27 << 8}`]
            //  int16[1] = ['0x180']
        */

    const bufferHead = new ArrayBuffer(2)
    const bufferBody = new ArrayBuffer(4)
    let intArrHead = new Uint8Array(bufferHead)
    if (typeCommand === '0x21') {
        switch(typeData) {
            case 'char':
                
                for(let i = 0; i < 2; i++) {
                    intArrHead[i] = command[i]
                }
                const encoder = new TextEncoder()
                const intArrBody = encoder.encode(command[2])
                let intChar = new Uint8Array(intArrHead.byteLength + intArrBody.byteLength)
                intChar.set(new Uint8Array(bufferHead), 0)
                intChar.set(intArrBody, intArrHead.byteLength)
                return intChar
            case 'uint8':
                let intArr8 = new Uint8Array(buffer)
                for(let i = 0; i < 2; i++) {
                    intArr8[i] = command[i]
                }
                intArr8[2] = command[2]
                return intArr8
            case 'uint16':
                let intArr16 = new Uint8Array(buffer)
                for(let i = 0; i < 2; i++) {
                    intArr16[i] = command[i]
                }
                intArr16 = new Uint16Array(buffer)
                intArr16[1] = command[2]
                return intArr16
            case 'uint32':

            console.log('intArrHead', intArrHead)

            for(let i = 0; i < 2; i++) {
                intArrHead[i] = command[i]
            }
            let intArrBody32 = new Uint32Array(bufferBody)
            intArrBody32[0] = command[2]
            let intArr32 = new Uint8Array(intArrHead.byteLength + intArrBody32.byteLength)
            intArr32.set(new Uint8Array(bufferHead), 0)
            intArr32.set(new Uint8Array(bufferBody), intArrHead.byteLength)
                return intArr32
            default:
                throw new Error('Invalid type')
        }
    } else {
        let intArr = new Uint8Array(buffer)
            for(let i = 0; i < 2; i++) {
                intArr[i] = command[i]
            }
        return intArr    
    }
}

const typeCommands = (type) => {
    switch(type) {
        case '10' : return 'settings';
        case '11' : return 'settings';
        case '12' : return 'values';
        case '21' : return 'settings';
    }
}

const bufferType = (typeUint, buffer) => {
    switch(typeUint) {
        case 'uint8': return new Uint8Array(buffer.slice(2))
        case 'uint16': return new Uint16Array(buffer.slice(2))
        case 'uint32': return new Uint8Array(buffer)
        default: return new Uint8Array(buffer.slice(2))
    }
}

const bufferSize = (value, typeCommand, typeData) => {
    const buffer = parseInt(value) > 255 || typeData === 'uint16' ? 4 : 3
    if (typeCommand === '0x10' || typeCommand === '0x11' || typeCommand === '0x12') return 2
    if (typeCommand === '0x21') return buffer
}

const loggerTextToNumber = (text) => {
    text = text.toLowerCase();
    switch(text) {
        case 'отключен': return 0;
        case 'только ошибки': return 1;
        case 'предупреждения': return 2;
        case 'полный': return 3;
        default: return 0;
    }
}    

export {onButtonClick, typeCommands, loggerTextToNumber, bufferSize, getViewType, bufferType}