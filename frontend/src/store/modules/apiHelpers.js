import axios from "axios";
import {initialButtonState} from "./initState.js";

const post = async (path, data, success_callback) => {
    await axios.post(`${path}`, data)
        .then(success_callback)
        .catch(error => {
            console.error('Error post request:', error)
        })
}

const getStoredData = async (filename, success_callback) => {
    await post("/api/get_init_data", {filename}, success_callback)
}
const saveFile = async (filename, data, btn, onReset) => {
    await post("/api/save_init_data", {filename, data}, (res) => {
        if(res.data.status === "success") {
            btn.loading = false;
            btn.disabled = false;
            btn.success = true;

            onReset()
        }
    })
}

export {post, getStoredData, saveFile}