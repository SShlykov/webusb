const initialButtonState = () => ({ loading: false, success: false, disabled: false});
const initialText = () => JSON.stringify({}, null, 2)
const initialObject = () => ({});
const initialError  = () => null;
const defaultAppConfingOpened = false
const initialCommandsText = () => "";

export {initialButtonState, initialText, initialObject, initialError, defaultAppConfingOpened, initialCommandsText}