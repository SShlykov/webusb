import {defaultAppConfingOpened} from "../initState.js";

const state = {
    appConfigOpened: defaultAppConfingOpened,
    menuOpened: false,
    theme: 'light'
};
const getters= {
    appConfigOpened:     (state) => state.appConfigOpened,
    menuOpened:          (state) => state.menuOpened,
    theme:               (state) => state.theme
};

const mutations = {
    setAppConfigOpened:     (state, updVal) => state.appConfigOpened = updVal,
    toggleAppConfigOpened:  (state) => state.appConfigOpened = !state.appConfigOpened,
    toggleMenuOpened:       (state) => state.menuOpened = !state.menuOpened,
    toggleTheme:            (state) => state.theme = state.theme === 'dark' ? 'light' : 'dark'
};

const actions = {};

export const store = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};