import {initialError, initialObject, initialText, initialButtonState} from "../initState.js";
import {getStoredData, saveFile} from "../apiHelpers.js";
import {onButtonClick} from "../helpers.js";

const state = {
    values:         initialObject(),
    valuesError:    initialError(),
    valuesIsOpen:   false,
    valuesText:     initialText(),
    valuesButton:   initialButtonState(),
};
const getters= {
    values:         (state) => state.values,
    valuesError:    (state) => state.valuesError,
    valuesIsOpen:   (state) => state.valuesIsOpen,
    valuesText:     (state) => state.valuesText,
    valuesButton:   (state) => state.valuesButton,
};

const mutations = {
    setValues:          (state, updVal) => state.values = updVal,
    setValuesError:     (state, updVal) => state.valuesError = updVal,
    toggleValuesIsOpen: (state) => state.valuesIsOpen = !state.valuesIsOpen,
    setValuesText:      (state, updVal) => state.valuesText = updVal,
    setValuesButton:    (state, updVal) => state.valuesButton = updVal,
    putValues: (state, newValues) => {
      if (newValues.id && newValues.key) {
          state.values.push(newValues)
      }
    },
    putValuesText: (state, newValues) => {
      if (newValues.id && newValues.key) {
          state.valuesText.push(newValues)
      }
    },
};

const actions = {
  initValues: async ({commit}) => {
    await getStoredData("values.json",
      response => {
        let values = response.data
          commit('setValues', values)
          commit('setValuesText', JSON.stringify(values, null, 2))
    })
  },
  setValues: ({commit}, values) => {
        commit('setValues', values)
        commit('setValuesText', JSON.stringify(values, null, 2))
    },
  setValuesText: ({commit}, e) => {
    try {
        let valuesText = e.target.value;
        commit('setValuesText', valuesText)
        commit('setValues', JSON.parse(valuesText))
        commit('setValuesError', null)
    } catch (e) {
          commit('setValuesError', e)
    }
  },
  loadFromDevice: ({commit, getters}, values) => {
    const newValues = {...getters.values, [values.key]: values.value};
    commit('putValies', newValues)
    commit('putValiesText', JSON.stringify(newValues, null, 2))
  },
  // Обновление данных которые изначально добавились в state из файла
  updateValues: async ({commit, getters}, values) => {
      const newParams = getters.values[0].params.map((param) => {
        if(param.key === values.key) {
          param.value = values.value
          return param
        }
        return param
      })
      const newValues = [{...getters.values[0], params: newParams}]
    commit('setValues', newValues)
    commit('setValuesText', JSON.stringify(newValues, null, 2))
  },
  onSaveValues: async ({commit, getters}) => {
    console.log('onSaveValues', getters.valuesText)
    onButtonClick(getters.valuesButton,
        () => saveFile("values.json", getters.valuesText, getters.valuesButton,
            () => commit('setValuesButton', initialButtonState())
        )
    )
}
};

export const store = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};