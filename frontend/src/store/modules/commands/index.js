import {initialCommandsText} from "../initState.js";

const state = {
    result: {},
    commandsText: '',
    commandSet: '',
};

const getters= {
    commandsText: (state) => state.commandsText,
    commandSet: (state) => state.commandSet,
};

const mutations = {
    setResult: (state, updVal) => state.result = updVal,
    setCommandsText: (state, updVal) => state.commandsText = updVal,
    setCommandSet: (state, updVal) => state.commandSet = updVal,
    putLog:     (state, newLog) =>  {
        if (newLog.text && newLog.date) {
            state.deviceLogs.push(newLog)
        }
    },
};

const actions = {
    handleTextUpdate: async ({commit}, value) => {
        try {
            await commit('setCommandsText', value)
        } catch (error) {
            await commit('setResult', {});
            console.error('error handle event read value', error);
        }
    },
    handleCommandSetUpdate: async ({commit}, value) => {
        try {
            await commit('setCommandSet', value)
        } catch (error) {
            await commit('setResult', {});
            console.error('error handle event read value', error);
        }
    },
}

export const store = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};