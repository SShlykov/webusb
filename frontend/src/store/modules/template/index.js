import {initialButtonState, initialError, initialObject, initialText} from "../initState.js";
import {getStoredData, saveFile} from "../apiHelpers.js";
import {onButtonClick} from "../helpers.js";

const state = {
    templates:         initialObject(),
    templatesError:    initialError(),
    templatesButton:   initialButtonState(),
    templatesText:     initialText(),
};
const getters= {
    templates:         (state) => state.templates,
    templatesError:    (state) => state.templatesError,
    templatesButton:   (state) => state.templatesButton,
    templatesText:     (state) => state.templatesText,
};

const mutations = {
    setTemplates:          (state, updVal) => state.templates = updVal,
    setTemplatesError:     (state, updVal) => state.templatesError = updVal,
    setTemplatesButton:    (state, updVal) => state.templatesButton = updVal,
    setTemplatesText:      (state, updVal) => state.templatesText = updVal,
};

const actions = {
    initTemplates: async ({commit}) => {
        await getStoredData("template.json",
            response => {
                let cfg = response.data
                commit('setTemplates', cfg)
                commit('setTemplatesText', JSON.stringify(cfg, null, 2))
            })
    },
    handleTemplatesTextUpdate: ({commit}, evt) => {
        try {
            let templates = evt.target.value;
            commit('setTemplatesText', templates)
            commit('setTemplates', JSON.parse(templates))
            commit('setTemplatesError', null)
        } catch (error) {
            commit('setTemplatesError', error)
        }
    },
    setTemplates: async ({commit}, templates) => {
        commit('setTemplates', templates)
        commit('setTemplatesText', JSON.stringify(templates, null, 2))
    },
    updateTemplates: async ({commit, getters}, settings) => {
        const new_templates = {...getters.templates, [settings.key]: settings.value};
        commit('setTemplates', new_templates)
        commit('setTemplatesText', JSON.stringify(new_templates, null, 2))
    },
    onSaveTemplates: async ({commit, getters}) => {
        onButtonClick(getters.templatesButton, () => saveFile(
                "template.json", getters.templatesText, getters.templatesButton,
                () => commit('setTemplatesButton', initialButtonState())
            )
        )
    }
};

export const store = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};