import {initialButtonState, initialError, initialObject, initialText} from "../initState.js";
import {getStoredData, saveFile} from "../apiHelpers.js";
import {onButtonClick} from "../helpers.js";

const state = {
    blocks:         initialObject(),
    blocksError:    initialError(),
    blocksButton:   initialButtonState(),
    blocksText:     initialText(),
};
const getters= {
    blocks:         (state) => state.blocks,
    blocksError:    (state) => state.blocksError,
    blocksButton:   (state) => state.blocksButton,
    blocksText:     (state) => state.blocksText,
};

const mutations = {
    setBlocks:          (state, updVal) => state.blocks = updVal,
    setBlocksError:     (state, updVal) => state.blocksError = updVal,
    setBlocksButton:    (state, updVal) => state.blocksButton = updVal,
    setBlocksText:      (state, updVal) => state.blocksText = updVal,
};

const actions = {
    initBlocks: async ({commit}) => {
        await getStoredData("blocks.json",
            response => {
                let cfg = response.data
                commit('setBlocks', cfg)
                commit('setBlocksText', JSON.stringify(cfg, null, 2))
            })
    },
    handleBlocksTextUpdate: ({commit}, evt) => {
        try {
            let blocks = evt.target.value;
            commit('setBlocksText', blocks)
            commit('setBlocks', JSON.parse(blocks))
            commit('setBlocksError', null)
        } catch (error) {
            commit('setBlocksError', error)
        }
    },
    onSaveBlocks: async ({commit, getters}) => {
        onButtonClick(getters.blocksButton, 
            () => saveFile("blocks.json", getters.blocksText, getters.blocksButton,
                () => commit('setBlocksButton', initialButtonState())
            )
        )
    }
};

export const store = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};