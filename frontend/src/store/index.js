import { createStore, createLogger } from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import {store as blocks} from './modules/blocks';
import {store as device} from './modules/device';
import {store as layout} from './modules/layout';
import {store as template} from './modules/template';
import {store as config} from './modules/config';
import {store as commands} from './modules/commands';
import {store as values} from './modules/values';

const debug = process.env.NODE_ENV !== 'production';
const plugins = debug ? [createLogger({})] : [];

plugins.push(createPersistedState({ storage: window.sessionStorage }));

export const store = createStore({
    plugins,
    modules: {
        blocks,
        device,
        layout,
        template,
        config,
        commands,
        values
    },
});

export function useStore() {
    return store;
}