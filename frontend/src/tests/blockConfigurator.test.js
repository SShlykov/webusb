import {mount, shallowMount} from '@vue/test-utils'
import Vuex, {createStore} from 'vuex'
import {createApp} from 'vue'
import {store as blocks} from '../store/modules/blocks'
import BlockConfigurator from "@cmp/BlockConfigurator.vue"

test('1 to be 1', () => {
  const store = createStore({...blocks})
  const blockConfigurator = createApp(BlockConfigurator)
  blockConfigurator.use(store)

  expect(1).toBe(1)
})

// test('renders a BlockConfigurator', () => {
//   const wrapper = mount(BlockConfigurator)

//   const container = wrapper.get('.mt-4 border rounded-md')
//   const title = container.get('div')

//   expect(title.text()).toContain('Конфигурация блоков страницы')
// })

// test('is not error BlockConfigurator', () => {
//   const wrapper = mount(BlockConfigurator)

//   const container = wrapper.get('.mt-4 border rounded-md')

//   expect(container.find('[type="button"]').exists()).toBe(true)
// })