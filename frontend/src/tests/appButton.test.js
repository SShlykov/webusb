import {mount} from '@vue/test-utils'
import AppButton from "@cmp/AppButton.vue";

test('Загрузка всего компонента AppButton', async () => {
  const wrapper = mount(AppButton)

  const container = wrapper.get('[type="button"]')
  const transition = container.get('[name="v-btn-label"]')
  
  await expect(transition.attributes('mode')).toBe('out-in')
})

test('Проверка сосотояния загрузки кнопки AppButton', async () => {
  const wrapper = mount(AppButton, {
    props: {
      loading: false,
      success: false,
    },
    methods: {
      onClick() {
        this.$emit('click');
      },
    }
  })

  await wrapper.setProps({loading: true})
  expect(wrapper.find('.spinner').isVisible()).toBe(true)
})

test('Проверка сосотояния после успешной загрузки кнопки AppButton', async () => {
  const wrapper = mount(AppButton, {
    props: {
      loading: false,
      success: false,
    },
    methods: {
      onClick() {
        this.$emit('click');
      },
    }
  })

  await wrapper.setProps({success: true})

  const span = wrapper.find('svg')
  expect(span.get('path').isVisible()).toBe(true)
})