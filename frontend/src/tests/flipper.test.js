import {mount} from '@vue/test-utils'
import Flipper from "@cmp/Flipper.vue";

test('Загрузка всего компонента Flipper', async () => {
  const wrapper = mount(Flipper)
  const container = wrapper.get('.flip')

  await expect(container.get('.flip-content').isVisible()).toBe(true)
  
})

test('Загрузка фронта компонента Flipper', async () => {
  const wrapper = mount(Flipper, {
    props: {
      reversed: '',
    },
  })

  const container = wrapper.get('.flip')

  await expect(container.get('.flip-front').isVisible()).toBe(true)

})

test('Загрузка бэка компонента Flipper', async () => {
  const wrapper = mount(Flipper, {
    props: {
      reversed: '',
    },
  })

  const container = wrapper.get('.flip')

  await wrapper.setProps({reversed: true})
  expect(container.get('.flip-back').isVisible()).toBe(true)

})