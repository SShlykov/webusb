import {mount} from '@vue/test-utils'
import RenderInputs from "../components/RenderInputs.vue";

test('Загрузка общего заголовка компонента RenderInputs', async () => {
  const wrapper = mount(RenderInputs, {
    props: {
      blocks: [{
          "id": 1,
          "title": "Общие настройки устройства",
          "params": [{"key": "serial_number", "title": "Серийный номер"}]
        }], 
      config: {"serial_number":    84712890},
      // inputChange: (e) => console.log(e)
    }
  })
  // const container = wrapper.get('.my-4')
  const title = wrapper.find('.bg-slate-100')

  expect(title.text()).toBe('Общие настройки устройства')
})

test('Загрузка заголовка внутри блока компонента RenderInputs', async () => {
  const wrapper = mount(RenderInputs, {
    props: {
      blocks: [{
          "id": 1,
          "title": "Общие настройки устройства",
          "params": [{"key": "serial_number", "title": "Серийный номер"}]
        }], 
      config: {"serial_number":    84712890},
      // inputChange: (e) => console.log(e)
    }
  })

  const title = wrapper.find('label')

  expect(title.text()).toBe('Серийный номер')
})

test('Проверка id у input в блоке компонента RenderInputs', async () => {
  const wrapper = mount(RenderInputs, {
    props: {
      blocks: [{
          "id": 1,
          "title": "Общие настройки устройства",
          "params": [{"key": "serial_number", "title": "Серийный номер"}]
        }], 
      config: {"serial_number":    84712890},
      // inputChange: (e) => console.log(e)
    }
  })

  const input = wrapper.find('input')
  expect(input.attributes('id')).toBe('serial_number')
})

test('Изменение значения в input, получение его в inputChange и замена значения в props компонента RenderInputs', async () => {
  const wrapper = mount(RenderInputs, {
    props: {
      blocks: [{
          "id": 1,
          "title": "Общие настройки устройства",
          "params": [{"key": "serial_number", "title": "Серийный номер"}]
        }], 
      config: {"serial_number": 84712890, "port_data_serv": 5162},
      inputChange: (e) => {
          const [block] = wrapper.props(['blocks'])
          const [param] = block.params

        const config = wrapper.props('config')
        wrapper.setProps({[config[param.key]]: e.target.value})
      }
    },
  })

  const [block] = wrapper.props(['blocks'])
  const [param] = block.params
  const config = wrapper.props('config')

  await wrapper.find('input').setValue('test7')
  console.log('config.key - console 2', wrapper.props('config'))
  expect(config[param.key]).toBe('test7')
})

// test('is close ConfigEditor', () => {
//   const wrapper = mount(ConfigEditor)

//   expect(wrapper.get('.editor-closed').isVisible()).toBe(true)
// })