import {mount} from '@vue/test-utils'
import ConfigEditor from "@cmp/ConfigEditor.vue";

test('Загрузка всего компонента ConfigEditor', async () => {
  const wrapper = mount(ConfigEditor)

  const container = wrapper.get('.truncate')
  const span = container.get('.px-2')

  await expect(span.text()).toContain('Открыть\\скрыть')
})

test('Проверка закрытия компонента ConfigEditor', async () => {
  const wrapper = mount(ConfigEditor, {
    props: {
      text: {
        "serial_number": 84712890,
        "log_level": 0,
        "movement_period": 40,
        "stopped_period": 120,
        "reg_1": "\"IP\",\"internet.beeline.ru\"",
        "addr_data_serv": "91.151.186.53"},
      input: () => {},
      isOpen: false,
      toggleEditor: !this.isOpen
    }
  })

  await expect(wrapper.get('.truncate').get('.editor-closed').isVisible()).toBe(true)
})

test('Проверка открытия компонента ConfigEditor', async () => {
  const wrapper = mount(ConfigEditor, {
    props: {
      text: {
        "serial_number": 84712890,
        "log_level": 0,
        "movement_period": 40,
        "stopped_period": 120,
        "reg_1": "\"IP\",\"internet.beeline.ru\"",
        "addr_data_serv": "91.151.186.53"},
      input: () => {},
      isOpen: false,
      toggleEditor: !this.isOpen
    }
  })

  await wrapper.setProps({isOpen: true})
  const container = wrapper.get('.truncate')
  console.log('isOpen - ConfigEditor', wrapper.props('isOpen'))
  expect(container.get('.editor-opened').isVisible()).toBe(true)
})