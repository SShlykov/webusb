import { allCommandValues } from "./data"

const isEmptyObject = obj => {
  if(obj === null) return true
  return Object.keys(obj).length === 0
}

const sleep = ms => new Promise(r => setTimeout(r, ms))

const scrollToTarget = ({targetElem, targetContainer, yOffset = 0}) => {
    const targetElemTop = targetElem ? targetElem.offsetTop : 0
    setTimeout(() => {
        if (targetElemTop) {
            targetContainer.scrollTop = targetElemTop + yOffset
        }
    }, 500)
}

const logLevel = (code) => {
  switch (code) {
      case 0: return 'Отключен';
      case 1: return 'Только ошибки';
      case 2: return 'Предупреждения';
      case 3: return 'Полный';
  }
  
}

const typeUint = (num) => {
  switch (num) {
      case '8': return 'uint8';
      case '16': return 'uint16';
      case '32': return 'uint32';
      default: return 'char';
  }
  
}

async function putAllCommands({device, type, action, params}) {
  console.log('params', params)
  params.map(async (param) => {
    // const readyCommand = `${type}, ${command}`
    const readySetting = {typeUint: param.type, command: `${type}, ${param.command}`}
    return await action({device: device, typeUint: readySetting.typeUint, command: readySetting.command})
  })
}

async function getConfigParams(paramSet, paramVal, ms) {
  if (paramSet ==='{}' || paramVal === '{}') {
    await sleep(ms)
    return await getConfigParams(paramSet, paramVal, ms)
  }
  if (JSON.parse(paramSet).length > 0 && JSON.parse(paramVal).length > 0) {
    return {paramSet: JSON.parse(paramSet), paramVal: JSON.parse(paramVal)}
  }
}

export {isEmptyObject, sleep, scrollToTarget, logLevel, putAllCommands, getConfigParams, typeUint}