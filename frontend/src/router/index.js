import {createRouter, createWebHistory} from "vue-router";
import Main from "../views/Main.vue";
import Diagnostic from "../views/Diagnostic.vue";
import Settings from "../views/Settings.vue";
import Commands from "../views/Commands.vue";
import Data from "../views/Data.vue";
import Map from "../views/Map.vue";
import FAQ from "../views/FAQ.vue";
import Infographics from "../views/Infographics.vue";
import DataList from "../views/DataList.vue";
// import Admin from '../views/About.vue';

const routes = [
    {
        path: "/",
        name: "Главная",
        icon: "ri-home-line",
        component: Main,
        visible: "always"
    },
    {
        path: "/f/map",
        name: "Инфографика",
        icon: "ri-map-line",
        component: Infographics,
        visible: "always"
    },
    {
        path: "/f/datalist",
        name: "Список данных",
        icon: "ri-database-line",
        component: DataList,
        visible: "always"
    },
    {
        path: "/f/device/settings",
        name: "Настройки",
        icon: "ri-settings-line",
        component: Settings,
        visible: "device"
    },
    {
        path: "/f/device/diagnostic",
        name: "Диагностика",
        icon: "ri-line-chart-line",
        component: Diagnostic,
        visible: "device"
    },
    {
        path: "/f/device/commands",
        name: "Команды",
        icon: "ri-terminal-box-line",
        component: Commands,
        visible: "device"
    },
    {
        path: "/f/device/data",
        name: "Данные",
        icon: "ri-database-2-line",
        component: Data,
        visible: "device"
    },
    {
        path: "/f/device/map",
        name: "Маршруты",
        icon: "ri-road-map-line",
        component: Map,
        visible: "device"
    },
    {
        path: "/f/faq",
        name: "FAQ",
        icon: "ri-question-line",
        component: FAQ,
        visible: "always"
    }
];

const routerHistory = createWebHistory();

const router = createRouter({
    history: routerHistory,
    routes
});

export {routes};
export default router;