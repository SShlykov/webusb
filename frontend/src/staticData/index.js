import moment from 'moment';
let colors = (opacity) => ({y: `rgba(229,229,119,${opacity})`, r: `rgba(173,66,66,${opacity})`, g: `rgba(0, 100, 100,${opacity})`})

let generateTimeStamps = (currentTime) => {
    let timeStamps = [];

    for (let i = 0; i < 25; i++) {
        timeStamps.push(currentTime.format("HH:mm"));
        currentTime.subtract(5, 'minutes');
    }

    return timeStamps;
}
let graphTimes =  generateTimeStamps(moment()).reverse()
let getVoltageColors = (value) =>  {
    let c = colors(1)
    if (value > 14 || value < 10) {
        return c.g;
    } else {
        return c.r;
    }
}
let voltageData = [10, 12.7, 13.5, 14, 12.5, 13.8, 13, 14, 12.9, 13.2, 13.6, 14, 12.8, 13.1, 13.7, 12.5, 14, 13.3, 12.6, 13.9, 12.7, 14, 13.4, 12.5, 13.5]
let voltageColors = voltageData.map(getVoltageColors)

let fuelData = [759.16, 857.6, 828.92, 742.49, 616.55, 564.01, 211.54, 211.27, 211.3, 895.8, 870.96, 867.61, 865.37, 843.34, 797.18, 790.42, 752.59, 842.12, 805.44, 791.74, 808.34, 809.04, 793.21, 749.95, 761.15]
let tempCData = [20, 21, 23, 19, 20, 21, 23, 23, 19, 16, 17, 21, 18, 18, 24, 25, 24, 19, 23, 24, 21, 23, 19, 24, 21]

let getPColor = (value) =>  {
    let c = colors(1)
    if (value > 1 || value < 0.4) {
        return c.r;
    } else {
        return c.g;
    }
}
let pData = [ 0.4, 0.95, 1.05, 0.92, 1.03, 1.07, 0.98, 0.99, 1.02, 1.04, 0.96, 1.01, 0.97, 1.08, 0.95, 1.05, 1.00, 0.98, 1.04, 0.99, 1.02, 1.03, 0.97, 0.98, 1.01]
let pColrs = pData.map(getPColor)

let getTempColor = (value) =>  {
    let c = colors(1)
    if (value > 93) {
        return c.r;
    } else if(value < 0) {
        return c.y;
    } else {
        return c.g;
    }
}
let tempData = [1,4,6,2,11,14,9,19,18,32,30,48,55,64,62,75,73,72,83,79,78,87,80,84,85]
let tempColrs = tempData.map(getTempColor)

let getColor = (value) =>  {
    let c = colors(1)
    if (value > 500) {
        return c.r;
    } else if(value < 200) {
        return c.g;
    } else {
        return c.y;
    }
}
let energyData = [ 182, 187, 181, 190, 194, 201, 205, 197, 210, 212, 215, 278, 240, 397, 430, 453, 402, 472, 426, 490, 207, 206, 190, 188, 191]
let energyColors = energyData.map(getColor)

export const energyChartData = {
    data: {
        labels: graphTimes,
        datasets: [
            {
                label: 'Потребляемая мощность, кВт/ч',
                data: energyData,
                borderColor: 'rgb(168,168,168)',
                backgroundColor: energyColors,
                fill: true,
                tension: 0.1
            },
        ],
    },
    options: {
        interaction: {
            mode: 'index',
            intersect: false
        },
        plugins: {
            legend: {
                display: false
            },
            annotation: {
                annotations: [
                    {
                        type: 'line',
                        mode: 'horizontal',
                        scaleID: 'y',
                        value: 200,
                        borderColor: colors(0.4).y,
                        borderWidth: 2,
                        label: {
                            enabled: true,
                            content: 'Минимальный порог',
                        },
                    },
                    {
                        type: 'line',
                        mode: 'horizontal',
                        scaleID: 'y',
                        value: 500,
                        borderColor: colors(0.4).r,
                        borderWidth: 2,
                        label: {
                            enabled: true,
                            content: 'Максимальный порог',
                        },
                    },
                ],
            },
        },
        scales: {
            x: {
                display: true,
                title: {
                    display: true
                }
            },
            y: {
                display: true,
                title: {
                    display: true,
                    text: 'Потребляемая мощность'
                },
                suggestedMin: 150,
                suggestedMax: 600
            }
        }
    },
}
export const tempOJData = {
    data: {
        labels: graphTimes,
        datasets: [
            {
                label: 'Температура охлождающей жидкости, °C',
                data: tempData,
                borderColor: 'rgb(168,168,168)',
                backgroundColor: tempColrs,
                fill: true,
                tension: 0.1
            },
        ],
    },
    options: {
        responsive: false,
        interaction: {
            mode: 'index',
            intersect: false
        },
        plugins: {
            legend: {
                display: false
            },
            annotation: {
                annotations: [
                    {
                        type: 'line',
                        mode: 'horizontal',
                        scaleID: 'y',
                        value: 93,
                        borderColor: colors(0.4).r,
                        borderWidth: 2,
                        label: {
                            enabled: true,
                            content: 'Максимальный порог',
                        },
                    },
                ],
            },
        },
        scales: {
            x: {
                display: true,
                title: {
                    display: true
                }
            },
            y: {
                display: true,
                title: {
                    display: true,
                    text: 'Температура охлождающей жидкости, °C'
                },
                suggestedMin: 0,
                suggestedMax: 100
            }
        }
    },
}
export const PData = {
    data: {
        labels: graphTimes,
        datasets: [
            {
                label: 'Давление, мПа',
                data: pData,
                borderColor: 'rgb(168,168,168)',
                backgroundColor: pColrs,
                fill: true,
                tension: 0.1
            },
        ],
    },
    options: {
        interaction: {
            mode: 'index',
            intersect: false
        },
        plugins: {
            legend: {
                display: false
            },
            annotation: {
                annotations: [
                    {
                        type: 'line',
                        mode: 'horizontal',
                        scaleID: 'y',
                        value: 0.4,
                        borderColor: colors(0.4).r,
                        borderWidth: 2,
                        label: {
                            enabled: true,
                            content: 'Минимальный порог',
                        },
                    },
                    {
                        type: 'line',
                        mode: 'horizontal',
                        scaleID: 'y',
                        value: 1,
                        borderColor: colors(0.4).r,
                        borderWidth: 2,
                        label: {
                            enabled: true,
                            content: 'Максимальный порог',
                        },
                    },
                ],
            },
        },
        scales: {
            x: {
                display: true,
                title: {
                    display: true
                }
            },
            y: {
                display: true,
                title: {
                    display: true,
                    text: 'Давление, мПа'
                },
                suggestedMin: 0,
                suggestedMax: 1.5
            }
        }
    },
}
export const tempChartData = {
    data: {
        labels: graphTimes,
        datasets: [
            {
                label: 'Температура',
                data: tempCData,
                borderColor: 'rgb(103,170,208)',
                backgroundColor: 'rgb(54, 162, 235)',
            },
        ]
    },
    options: {
        responsive: false,
        interaction: {
            intersect: false,
        },
        scales: {
            x: {
                display: true,
                title: {
                    display: true
                }
            },
            y: {
                display: true,
                title: {
                    display: true,
                    text: 'Температура'
                },
                suggestedMin: 0,
                suggestedMax: 40
            }
        }
    },
}
export const fuelChartData = {
    data: {
        labels: graphTimes,
            datasets: [
            {
                label: 'Объем топлива',
                data: fuelData,
                borderColor: 'rgb(103,170,208)',
                backgroundColor: 'rgb(54, 162, 235)',
            },
        ]
    },
    options: {
        responsive: true,
            interaction: {
            intersect: false,
        },
        scales: {
            x: {
                display: true,
                    title: {
                    display: true
                }
            },
            y: {
                display: true,
                    title: {
                    display: true,
                        text: 'Объем топлива, л'
                },
                suggestedMin: 0,
                suggestedMax: 200
            }
        }
    },
}
export const VoltData = {
    data: {
        labels: graphTimes,
        datasets: [
            {
                label: 'Заряд АКБ, В',
                data: voltageData,
                borderColor: 'rgb(168,168,168)',
                backgroundColor: voltageColors,
                fill: true,
                tension: 0.1
            },
        ],
    },
    options: {
        interaction: {
            mode: 'index',
            intersect: false
        },
        plugins: {
            legend: {
                display: false
            },
            annotation: {
                annotations: [
                    {
                        type: 'line',
                        mode: 'horizontal',
                        scaleID: 'y',
                        value: 10,
                        borderColor: colors(0.4).r,
                        borderWidth: 2,
                        label: {
                            enabled: true,
                            content: 'Минимальный порог',
                        },
                    },
                    {
                        type: 'line',
                        mode: 'horizontal',
                        scaleID: 'y',
                        value: 14,
                        borderColor: colors(0.4).r,
                        borderWidth: 2,
                        label: {
                            enabled: true,
                            content: 'Максимальный порог',
                        },
                    },
                ],
            },
        },
        scales: {
            x: {
                display: true,
                title: {
                    display: true
                }
            },
            y: {
                display: true,
                title: {
                    display: true,
                    text: 'Заряд АКБ, В'
                },
                suggestedMin: 0,
                suggestedMax: 30
            }
        }
    },
}