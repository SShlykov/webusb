import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  resolve: { alias: { 
    '@': '/src',
    '@cmp': '/src/components' 
  } },
  plugins: [vue()],
  server: {
    port: 8000,
    host: "0.0.0.0"
  },
  test: {
    coverage: {
      provider: 'v8' // or 'istanbul'
    },
    globals: true,
    environment: "happy-dom",
  },
})
