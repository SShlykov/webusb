package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"time"
)

type Route struct {
	Path    string `json:"path"`
	BackEnd string `json:"backend"`
	Role    string `json:"role"`
}

var SecretKey = []byte("your-256-bit-secret")
var BasePath = "/"

func main() {
	port := 80
	httpsPort := 443
	e := echo.New()
	e.Use(middleware.Recover())

	initReverseProxy(e)

	go func() {
		httpCfg := http.Server{
			Addr:        fmt.Sprintf(":%d", port),
			Handler:     e,
			ReadTimeout: 30 * time.Second, // customize http.Server timeouts
		}

		if err := httpCfg.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}()

	s := http.Server{
		Addr:      fmt.Sprintf(":%d", httpsPort),
		Handler:   e, // set Echo as handler
		TLSConfig: &tls.Config{
			//MinVersion: 1, // customize TLS configuration
		},
		ReadTimeout: 30 * time.Second, // use custom timeouts
	}

	if err := s.ListenAndServeTLS("/etc/ssl/certs/railsolar.crt", "/etc/ssl/private/railsolar.key"); err != http.ErrServerClosed {
		log.Fatal(err)
	}
}

func initReverseProxy(e *echo.Echo) {
	routeList := getRoutes()

	for i := 0; i < len(routeList); i++ {
		route := routeList[i]
		fmt.Printf("%v\n", route)
		backend, _ := url.Parse(route.BackEnd)
		proxy := httputil.NewSingleHostReverseProxy(backend)

		e.Group(route.Path, func(next echo.HandlerFunc) echo.HandlerFunc {
			return func(context echo.Context) error {
				return wakeGroup(context, route, backend, proxy)
			}
		})
	}
}

func wakeGroup(context echo.Context, route Route, backend *url.URL, proxy *httputil.ReverseProxy) error {
	req := context.Request()
	res := context.Response().Writer

	if !((route.Role == "") || (route.Role == "*")) && !isAuthenticated(req) {
		err := context.Redirect(http.StatusTemporaryRedirect, BasePath)
		if err != nil {
			return nil
		}
		return nil
	}

	req.Host = backend.Host
	req.URL.Host = backend.Host
	req.URL.Scheme = backend.Scheme

	path := req.URL.Path
	//req.URL.Path = strings.TrimLeft(path, route.Path)
	req.URL.Path = path

	proxy.ServeHTTP(res, req)
	return nil
}

func isAuthenticated(r *http.Request) bool {
	//TODO: JWT AUTH implementation
	return true
}

func getRoutes() []Route {
	data, err := os.ReadFile("routes.json")
	if err != nil {
		log.Fatalf("Не удалось прочитать файл конфигурации маршрутов: %v", err)
	}

	var routes []Route
	if err := json.Unmarshal(data, &routes); err != nil {
		log.Fatalf("Не удалось проанализировать файл конфигурации маршрутов: %v", err)
	}

	return routes
}
