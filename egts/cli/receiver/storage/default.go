package storage

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
)

type LogConnector struct{}

func (c LogConnector) Init(cfg map[string]string) error {
	return nil
}

func (c LogConnector) Save(msg interface{ ToBytes() ([]byte, error) }) error {
	jsonPkg, err := json.MarshalIndent(msg, "", "    ")
	if err != nil {
		return err
	}

	jsonString := string(jsonPkg)

	cleanedJsonString := strings.ReplaceAll(jsonString, "\n", "")

	send([]byte(cleanedJsonString))
	log.Info("Packet exported")

	return nil
}

func (c LogConnector) Close() error {
	return nil
}

func send(data []byte) {
	url := "http://backend:8000/api_communications/save/raw_points"

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))

	if err != nil {
		fmt.Println("Error creating request:", err)
		return
	}

	req.Header.Set("Content-Type", "application/json") // set the content type header to JSON

	client := &http.Client{}
	resp, err := client.Do(req) // send the request
	if err != nil {
		fmt.Println("Error sending request:", err)
		return
	}
	defer resp.Body.Close()

	// read the response body
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		fmt.Println("Error reading response body:", err)
		return
	}
}
