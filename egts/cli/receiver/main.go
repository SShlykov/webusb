package main

import (
	"flag"

	"172.25.78.108/SShlykov/egts/cli/receiver/config"
	"172.25.78.108/SShlykov/egts/cli/receiver/server"
	"172.25.78.108/SShlykov/egts/cli/receiver/storage"
	log "github.com/sirupsen/logrus"
)

func main() {
	var err error
	var cfg config.Settings
	cfgFilePath := ""
	flag.StringVar(&cfgFilePath, "c", "", "Конфигурационный файл")
	flag.Parse()

	if cfgFilePath == "" {
		log.Fatalf("Не задан путь до конфига")
	}

	cfg, err = config.New(cfgFilePath)
	if err != nil {
		log.Fatalf("Ошибка парсинга конфига: %v", err)
	}

	log.SetLevel(cfg.GetLogLevel())

	// TODO: оставить дефолтное хранилище
	storages := storage.NewRepository()
	if err = storages.LoadStorages(cfg.Store); err != nil {
		store := storage.LogConnector{}
		if err = store.Init(nil); err != nil {
			log.Fatal(err)
		}

		storages.AddStore(store)
		defer store.Close()
	}

	srv := server.New(cfg.GetListenAddress(), cfg.GetEmptyConnTTL(), storages)

	log.Debugf("EGTS server started\n")
	srv.Run()
}
