package main

import (
	"fmt"
	"strings"
)

// [SrAdSensorsData: content]: []byte{0x0, 0x29, 0x7, 0x48, 0x10, 0x0, 0xbc, 0x5f, 0x0, 0x8, 0x9, 0x0}
// [SrAdSensorsData: flagBits]: "00000000"
// [SrAdSensorsData: content]: []byte{0x0, 0x29, 0x7, 0x48, 0x10, 0x0, 0xbc, 0x5f, 0x0, 0x8, 0x9, 0x0}
// [SrAdSensorsData: flagBits]: "00000000"
// [SrAdSensorsData: content]: []byte{0x0, 0x29, 0x7, 0x48, 0x10, 0x0, 0xbc, 0x5f, 0x0, 0x2, 0x9, 0x0}
// [SrAdSensorsData: flagBits]: "00000000"
// [SrAdSensorsData: content]: []byte{0x0, 0x29, 0x7, 0x48, 0x10, 0x0, 0xb0, 0x5f, 0x0, 0x2, 0x9, 0x0}
// [SrAdSensorsData: flagBits]: "00000000"
// [SrAdSensorsData: content]: []byte{0x0, 0x29, 0x7, 0x4a, 0x10, 0x0, 0xbc, 0x5f, 0x0, 0x2, 0x9, 0x0}
// [SrAdSensorsData: flagBits]: "00000000"
// [SrAdSensorsData: content]: []byte{0x0, 0x29, 0x7, 0x4a, 0x10, 0x0, 0xb0, 0x5f, 0x0, 0x2, 0x9, 0x0}
// [SrAdSensorsData: flagBits]: "00000000"

//байты: [
//1 0 0 11 0 158 1 1 0 1 129
//58 0 46 60 129 233 3 0 0 2 2
//16 26 0 188 73 254 25 0 123 140 156 128 57 228 117 131 0 128 81 0 0 0 0 5 133 0 0 0 0 17 4 0 10 5 0 25 19 4 0 1 55 4 0
//18 12 0 0 41 7 72 16 0 188 95 0 8 9 0 58 0 47 60 129 233 3 0 0 2 2 16 26 0 248 73 254 25 0 123 140 156 128 57 228 117 131 0 128 81 0 0 0 0 5 133 0 0 0 0 17 4 0 10 5 0 24 19 4 0 1 55 4 0
//18 12 0 0 41 7 72 16 0 188 95 0 8 9 0 58 0 48 60 129 233 3 0 0 2 2 16 26 0 52 74 254 25 0 123 140 156 128 57 228 117 131 0 128 81 0 0 0 0 5 133 0 0 0 0 17 4 0 10 5 0 26 19 4 0 1 55 4 0
//18 12 0 0 41 7 72 16 0 188 95 0 2 9 0 58 0 49 60 129 233 3 0 0 2 2 16 26 0 112 74 254 25 0 123 140 156 128 57 228 117 131 0 128 81 0 0 0 0 5 133 0 0 0 0 17 4 0 10 5 0 24 19 4 0 1 55 4 0
//18 12 0 0 41 7 72 16 0 176 95 0 2 9 0 58 0 50 60 129 233 3 0 0 2 2 16 26 0 172 74 254 25 0 123 140 156 128 57 228 117 131 0 128 81 0 0 0 0 5 133 0 0 0 0 17 4 0 10 5 0 26 19 4 0 1 55 4 0
//18 12 0 0 41 7 74 16 0 188 95 0 2 9 0 58 0 51 60 129 233 3 0 0 2 2 16 26 0 232 74 254 25 0 123 140 156 128 57 228 117 131 0 128 81 0 0 0 0 5 133 0 0 0 0 17 4 0 10 5 0 26 19 4 0 1 55 4 0
//18 12 0 0 41 7 74 16 0 176 95 0 2 9 0 68 161]
//Значение дискретов: 41

func main() {
	bytes := [][]byte{
		[]byte{0x0, 0x29, 0x7, 0x48, 0x10, 0x0, 0xbc, 0x5f, 0x0, 0x8, 0x9, 0x0},
		[]byte{0x0, 0x29, 0x7, 0x48, 0x10, 0x0, 0xbc, 0x5f, 0x0, 0x8, 0x9, 0x0},
		[]byte{0x0, 0x29, 0x7, 0x48, 0x10, 0x0, 0xbc, 0x5f, 0x0, 0x2, 0x9, 0x0},
		[]byte{0x0, 0x29, 0x7, 0x48, 0x10, 0x0, 0xb0, 0x5f, 0x0, 0x2, 0x9, 0x0},
		[]byte{0x0, 0x29, 0x7, 0x4a, 0x10, 0x0, 0xbc, 0x5f, 0x0, 0x2, 0x9, 0x0},
		[]byte{0x0, 0x29, 0x7, 0x4a, 0x10, 0x0, 0xb0, 0x5f, 0x0, 0x2, 0x9, 0x0},
	}
	for _, item := range bytes {
		intList := []int{}

		for _, y := range item {
			intList = append(intList, int(y))
		}

		fmt.Printf("%#v\n", intList)
	}
}

func makeNumSice(pack string) (bytes []byte) {
	strBytes := strings.Split(pack, " ")
	for _, str := range strBytes {
		var b int
		fmt.Sscan(str, &b)
		bytes = append(bytes, byte(b))
	}
	return
}
