package logs

import (
	"fmt"
	"gitlab.com/SShlykov/webusb/lib/db"
	"net/http"
	"time"
)

func GetColorByLog(status string) string {
	switch status {
	case "INFO":
		return "\033[1;30m"
	case "WARN":
		return "\033[1;33m"
	case "ERR ":
		return "\033[1;31m"
	case "DEBG":
		return "\033[1;32m"
	default:
		return "\033[1;34m"
	}
}

func Log(status, text string) {
	currentTime := time.Now()
	color := GetColorByLog(status)
	reset := "\033[0m"

	fmt.Printf(
		"[%s | %s%s%s]: %s\n",
		currentTime.Format("2006-01-02 15:04:05"),
		color, status, reset, text)
}

func checkDb() (err error) {
	err = db.Db177Ent.Raw("select 1 as test").Error
	return
}

func InitLog(port string) {
	apiVersion := "v1.0"
	dbStatus := "connected"
	err := checkDb()
	if err != nil {
		dbStatus = fmt.Sprintf("not connected: %#v", err)
	}
	host := "localhost"

	Log("INFO", "----------------------------------------")
	Log("INFO", "API Startup Information:")
	Log("INFO", "----------------------------------------")
	Log("INFO", fmt.Sprintf("API Version: %s", apiVersion))
	Log("INFO", fmt.Sprintf("Listening on Port: http://%s:%s", host, port))
	Log("INFO", fmt.Sprintf("Database Connection Status: %s", dbStatus))
	Log("INFO", "----------------------------------------")
	Log("INFO", "API is starting...")
	Log("INFO", "----------------------------------------")
}

func LogIncomingRequest(r *http.Request) {
	ip := r.RemoteAddr
	url := r.URL
	userAgent := r.UserAgent()
	cookies := ""
	query := ""
	body := ""

	for key, value := range r.URL.Query() {
		query += fmt.Sprintf(" %s=%s;", key, value)
	}

	for _, cookie := range r.Cookies() {
		// cookies += fmt.Sprintf(" %s=%s;", cookie.Name, cookie.Value)
		cookies += fmt.Sprintf(" %s=#;", cookie.Name)
	}

	r.ParseForm()
	for key, value := range r.Form {
		body += fmt.Sprintf(" %s=%s;", key, value)
	}

	log_string := fmt.Sprintf("inc request %s - ip: %s, user-agent: %s, cookies: %s, query: %s, body: %s", url, ip, userAgent, cookies, query, body)

	Log("DEBG", log_string)
}
