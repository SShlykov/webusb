package device

import (
	"encoding/json"
	"fmt"
)

type Device struct {
	SerialNumber   uint32 `json:"serial_number"`
	LogLevel       uint8  `json:"log_level"`
	MovementPeriod uint16 `json:"movement_period"`
	StoppedPeriod  uint16 `json:"stopped_period"`
	Reg1           []byte `json:"reg_1"`
	AddrDataServ   []byte `json:"addr_data_serv"`
	PortDataServ   []byte `json:"port_data_serv"`
	Reg2           []byte `json:"reg_2"`
	AddrHelpServ   []byte `json:"addr_help_serv"`
	PortHelpServ   []byte `json:"port_help_serv"`
	ConnTimeout    uint16 `json:"conn_timeout"`
	GpsSearchTime  uint16 `json:"gps_search_time"`
	GpsSattelite   uint8  `json:"gps_sattelite"`
	GpsFixes       uint8  `json:"gps_fixes"`
}

type DeviceIntermediate struct {
	SerialNumber   uint32 `json:"serial_number"`
	LogLevel       uint8  `json:"log_level"`
	MovementPeriod uint16 `json:"movement_period"`
	StoppedPeriod  uint16 `json:"stopped_period"`
	Reg1           string `json:"reg_1"`
	AddrDataServ   string `json:"addr_data_serv"`
	PortDataServ   string `json:"port_data_serv"`
	Reg2           string `json:"reg_2"`
	AddrHelpServ   string `json:"addr_help_serv"`
	PortHelpServ   string `json:"port_help_serv"`
	ConnTimeout    uint16 `json:"conn_timeout"`
	GpsSearchTime  uint16 `json:"gps_search_time"`
	GpsSattelite   uint8  `json:"gps_sattelite"`
	GpsFixes       uint8  `json:"gps_fixes"`
}

func toJsonDevice(s string) (*Device, error) {
	var deviceIntermediate DeviceIntermediate
	err := json.Unmarshal([]byte(s), &deviceIntermediate)
	if err != nil {
		return nil, err
	}

	reg1Bytes := []byte(deviceIntermediate.Reg1)
	addrDataServBytes := []byte(deviceIntermediate.AddrDataServ)
	portDataServBytes := []byte(deviceIntermediate.PortDataServ)
	reg2Bytes := []byte(deviceIntermediate.Reg2)
	addrHelpServBytes := []byte(deviceIntermediate.AddrHelpServ)
	portHelpServBytes := []byte(deviceIntermediate.PortHelpServ)

	device := Device{
		SerialNumber:   deviceIntermediate.SerialNumber,
		LogLevel:       deviceIntermediate.LogLevel,
		MovementPeriod: deviceIntermediate.MovementPeriod,
		StoppedPeriod:  deviceIntermediate.StoppedPeriod,
		Reg1:           reg1Bytes,
		AddrDataServ:   addrDataServBytes,
		PortDataServ:   portDataServBytes,
		Reg2:           reg2Bytes,
		AddrHelpServ:   addrHelpServBytes,
		PortHelpServ:   portHelpServBytes,
		ConnTimeout:    deviceIntermediate.ConnTimeout,
		GpsSearchTime:  deviceIntermediate.GpsSearchTime,
		GpsSattelite:   deviceIntermediate.GpsSattelite,
		GpsFixes:       deviceIntermediate.GpsFixes,
	}

	return &device, nil
}

func toUSBFormat(device *Device) ([]byte, error) {
	usbData, err := json.Marshal(device)
	if err != nil {
		return nil, err
	}

	usbCmdSetSetting := []byte{0x21}
	usbData = append(usbCmdSetSetting, usbData...)

	return usbData, nil
}

func device() {
	s := `{ "serial_number": 84712890, "log_level": 0, "movement_period": 40, "stopped_period": 120, "reg_1": "IP,internet.beeline.ru", "addr_data_serv": "91.151.186.53", "port_data_serv": "5162", "reg_2": "IP,internet.beeline.ru", "addr_help_serv": "91.151.186.53", "port_help_serv": "5162", "conn_timeout": 120, "gps_search_time": 120, "gps_sattelite": 8, "gps_fixes": 20 }`

	device, err := toJsonDevice(s)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	usbData, err := toUSBFormat(device)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	fmt.Println("USB Data:", usbData)
}
