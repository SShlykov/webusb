package controllers

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

func TestPostConf(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/api/save_init_data", strings.NewReader(`{"filename": "config.json","data": "test data"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	// Случай удачной обработки запроса
	if assert.NoError(t, PostConf(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
		response := struct {
			Status   string `json:"status"`
			Error    string `json:"error,omitempty"`
			DataSize int    `json:"data_size,omitempty"`
		}{}
		json.NewDecoder(rec.Body).Decode(&response)

		assert.Equal(t, "success", response.Status)
		assert.Equal(t, 9, response.DataSize)
		// удалить тестовый файл после завершения теста
		os.Remove("../config/config.json")
	}

	// Случай, когда тело запроса имеет некорректный формат
	req = httptest.NewRequest(http.MethodPost, "/", strings.NewReader(`{"bad_json_format"`))
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	if assert.Error(t, PostConf(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
	}
}
