package controllers

import (
	"bytes"
	"encoding/json"
	"github.com/labstack/echo/v4"
	"gitlab.com/SShlykov/webusb/lib/device"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetWebUsb(t *testing.T) {
	testDevice := device.Device{}

	payload, err := json.Marshal(testDevice)
	if err != nil {
		t.Fatalf("could not marshal test device: %v", err)
	}

	e := echo.New()
	req, err := http.NewRequest(echo.POST, "/usb", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatalf("could not create request: %v", err)
	}
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if err := GetWebUsb(c); err != nil {
		t.Fatalf("could not handle request: %v", err)
	}

	if status := rec.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v, want %v", status, http.StatusOK)
	}

	// Дополнительные проверки, если есть конкретный ожидаемый ответ или другие детали логики.
}
