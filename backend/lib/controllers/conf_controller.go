package controllers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
	"os"
)

type FileData struct {
	Filename string `json:"filename"`
	Data     string `json:"data"`
}

type requestPayload struct {
	Filename string `json:"filename"`
}

type responseData struct {
	Status   string `json:"status"`
	Error    string `json:"error,omitempty"`
	DataSize int    `json:"data_size,omitempty"`
}

func GetConfig(c echo.Context) error {
	payload := &requestPayload{}
	if err := c.Bind(payload); err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"error": "Failed to decode request body",
		})
	}

	filename := payload.Filename

	content, err := os.ReadFile(fmt.Sprintf("../config/%s", filename))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"error": "Failed to open file",
		})
	}

	return c.Blob(http.StatusOK, "application/octet-stream", content)
}

// PostConf функция обрабатывает HTTP запросы на создание файла конфигурации.
// Принимает на вход JSON объект с именем файла и данными.
// В случае успешного создания файла в ответ отправляется JSON объект
// с статусом "success" и размером записанных данных.
// В случае ошибки в ответ отправляется JSON объект с ошибкой и статусом "failed".
// Возможные ошибки:
// - Некорректный формат тела запроса
// - Ошибка при записи файла
func PostConf(c echo.Context) error {
	var fileData FileData
	response := responseData{}

	if err := c.Bind(&fileData); err != nil {
		response.Status = "failed"
		response.Error = "Invalid request body"
		return c.JSON(http.StatusBadRequest, response)
	}

	filename := fmt.Sprintf("../config/%s", fileData.Filename)
	dataBytes := []byte(fileData.Data)
	if err := os.WriteFile(filename, dataBytes, 0644); err != nil {
		response.Status = "failed"
		response.Error = "Error writing to file"
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Status = "success"
	response.DataSize = len(dataBytes)
	return c.JSON(http.StatusCreated, response)
}
