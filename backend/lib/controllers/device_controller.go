package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"io"
	"net/http"
)

// Pair представляет пару ключ/значение.
type Pair struct {
	Key   string      `json:"key"`
	Value interface{} `json:"value"`
}

// Data содержит набор пар ключ/значение.
type Data struct {
	Pairs []Pair `json:"data"`
}

// PostDevice обрабатывает HTTP POST-запрос, принимая JSON-тело с парами ключ/значение.
// Функция возвращает строку с командами USB в формате:
//
//	USB_CMD_SET_SETTING <Key> <Value>
//
// Возможные ошибки:
//   - StatusInternalServerError (500), если произошла ошибка при чтении тела запроса.
//   - StatusBadRequest (400), если произошла ошибка при десериализации JSON.
func PostDevice(c echo.Context) error {
	body, err := io.ReadAll(c.Request().Body)
	if err != nil {
		return c.String(http.StatusInternalServerError, "Error reading request body")
	}

	var data Data
	err = json.Unmarshal(body, &data)
	if err != nil {
		return c.String(http.StatusBadRequest, fmt.Sprintf("Error unmarshaling JSON %v", err))
	}

	result := ""
	for _, pair := range data.Pairs {
		result += fmt.Sprintf("USB_CMD_SET_SETTING %s %s\n", pair.Key, pair.Value)
	}

	return c.String(http.StatusOK, result)
}
