package controllers

import (
	"bytes"
	"github.com/labstack/echo/v4"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPostDevice(t *testing.T) {
	e := echo.New()
	data := `{"data": [{"key": "key1", "value": "value1"}, {"key": "key2", "value": "value2"}]}`
	req := httptest.NewRequest(http.MethodPost, "/post", bytes.NewReader([]byte(data)))
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if err := PostDevice(c); err != nil {
		t.Fatalf("Post failed with error: %v", err)
	}

	expected := "USB_CMD_SET_SETTING key1 value1\nUSB_CMD_SET_SETTING key2 value2\n"
	if got := rec.Body.String(); got != expected {
		t.Errorf("Expected result %q but got %q", expected, got)
		t.Logf("Response code: %d", rec.Code)
	}
}
