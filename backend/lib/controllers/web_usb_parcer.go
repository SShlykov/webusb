// Package controllers содержит функции для работы с USB-устройствами через веб-интерфейс.
package controllers

// GetWebUsb обрабатывает HTTP-запросы на получение данных USB-устройства, работая с Echo фреймворком.
// Функция читает тело запроса и пытается распарсить его как JSON в структуру device.Device.
// Полученные данные затем логируются.
//
// Параметры:
//   c - контекст Echo, содержащий информацию о запросе и ответе.
//
// Пример использования:
//
//   e := echo.New()
//   e.POST("/usb", GetWebUsb)

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/SShlykov/webusb/lib/device"
	"gitlab.com/SShlykov/webusb/lib/logs"
	"io"
	"net/http"
)

func GetWebUsb(c echo.Context) error {
	body, _ := io.ReadAll(c.Request().Body)

	var payload device.Device
	json.Unmarshal(body, &payload)

	logs.Log("INFO", fmt.Sprintf("Received data: %+v", payload))

	return c.String(http.StatusOK, "ok")
}
