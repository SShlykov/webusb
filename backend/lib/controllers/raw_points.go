package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/SShlykov/webusb/lib/models"
	"net/http"
)

type lastMahineRequest struct {
	IMEI int64 `json:"imei"`
}

type listMahineRequest struct {
	IMEI     int64  `json:"imei"`
	DtStart  string `json:"dt_start"`
	DtFinish string `json:"dt_finish"`
}
type pageError struct {
	Status string `json:"status"`
	error  string `json:"error"`
}
type listResponce struct {
	Data []models.MTSOLocationEGTS `json:"data"`
}

func ListPoints(c echo.Context) error {
	jsonMap := make(map[string]string)
	err := json.NewDecoder(c.Request().Body).Decode(&jsonMap)
	if err != nil {
		return err
	}

	list := models.ListRawPoints(jsonMap["dt_start"], jsonMap["dt_finish"])
	return c.JSON(http.StatusOK, list)
}

func GetPoints(c echo.Context) error {
	var jsonMap map[string]interface{}
	json.NewDecoder(c.Request().Body).Decode(&jsonMap)

	err := models.SaveRawPoint(jsonMap)
	if err != nil {
		fmt.Println(err)
		return err
	}

	return c.String(http.StatusOK, "ok")
}

func GetMTSOLocations(c echo.Context) error {
	var locations []models.MTSOLocationEGTS
	var lstMR listMahineRequest
	if err := json.NewDecoder(c.Request().Body).Decode(&lstMR); err != nil {
		return c.JSON(http.StatusBadRequest, "Неверный формат данных запроса")
	}

	models.GetMTSOLocationsBetweenDates(lstMR.IMEI, lstMR.DtStart, lstMR.DtFinish, &locations)

	return c.JSON(http.StatusOK, listResponce{Data: locations})
}

func GetLastMachineInfo(c echo.Context) error {
	var lmr lastMahineRequest
	json.NewDecoder(c.Request().Body).Decode(&lmr)

	mtsoInfo, err := models.GetLastMTSOInfo(lmr.IMEI)

	if err != nil {
		return c.JSON(http.StatusNoContent, pageError{Status: "не найдено", error: err.Error()})
	}

	return c.JSON(http.StatusOK, mtsoInfo)
}
