package db

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
	DATABASE = "egts_points"
	USERNAME = "db_admin"
	HOSTNAME = "172.25.78.167"
	PASSWORD = "8FWrDX6nB9zCreE5"
	PORT     = 5437
)

var DB *gorm.DB
var err error

func Connect() {
	dbURL := fmt.Sprintf("postgres://%s:%s@%s:%d/%s", USERNAME, PASSWORD, HOSTNAME, PORT, DATABASE)
	DB, err = gorm.Open(postgres.Open(dbURL), &gorm.Config{})

	if err != nil {
		fmt.Println(err.Error())
		panic("Can't connect to DB!")
	}
}
