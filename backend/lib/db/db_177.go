package db

import (
	"fmt"
	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"time"
)

type Db177Config struct {
	DATABASE string `mapstructure:"DB177_DATABASE"`
	USERNAME string `mapstructure:"DB177_USERNAME"`
	HOSTNAME string `mapstructure:"DB177_HOSTNAME"`
	PASSWORD string `mapstructure:"DB177_PASSWORD"`
	PORT     int    `mapstructure:"DB177_PORT"`
}

var Db177Ent *gorm.DB
var Db177Err error

func Connect177Db() {
	var config Db177Config

	config, Db177Err = loadDb177Config(".")
	if Db177Err != nil {
		log.Fatal("cannot load config:", err)
	}

	dbURL := fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable&application_name=backend", config.USERNAME, config.PASSWORD, config.HOSTNAME, config.PORT, config.DATABASE)
	Db177Ent, Db177Err = gorm.Open(postgres.Open(dbURL), &gorm.Config{})
	if Db177Err != nil {
		fmt.Println(Db177Err.Error())
		panic("Can't connect to DB!")
	}

	sqlDB, _ := Db177Ent.DB()
	sqlDB.SetMaxIdleConns(10)
	sqlDB.SetMaxOpenConns(100)
	sqlDB.SetConnMaxLifetime(time.Hour)
}

func loadDb177Config(path string) (config Db177Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("env")
	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}
