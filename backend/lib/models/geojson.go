package models

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
)

type GeoPoint struct {
	Lng float64 `json:"lng"`
	Lat float64 `json:"lat"`
}

func (p *GeoPoint) String() string {
	return fmt.Sprintf("SRID=4326;POINT(%v %v)", p.Lng, p.Lat)
}

func (p *GeoPoint) Scan(val interface{}) error {
	var geojsonStruct struct {
		Type        string    `json:"type"`
		Coordinates []float64 `json:"coordinates"`
	}

	switch v := val.(type) {
	case []byte:
		// Если это байты, скорее всего, это строка JSON, которую нужно распарсить
		if err := json.Unmarshal(v, &geojsonStruct); err != nil {
			return err
		}
	case string:
		// Если это строка, скорее всего, это строка JSON, которую нужно распарсить
		if err := json.Unmarshal([]byte(v), &geojsonStruct); err != nil {
			return err
		}
	default:
		return errors.New("unsupported data type for GeoPoint")
	}

	// Проверяем, что GeoJSON содержит правильный тип геометрии и координаты
	if geojsonStruct.Type != "Point" || len(geojsonStruct.Coordinates) != 2 {
		return errors.New("invalid GeoJSON data for GeoPoint")
	}

	p.Lng = geojsonStruct.Coordinates[0]
	p.Lat = geojsonStruct.Coordinates[1]

	return nil
}

func (p GeoPoint) Value() (driver.Value, error) {
	return p.String(), nil
}
