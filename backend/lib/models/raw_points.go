package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.com/SShlykov/webusb/lib/db"
	"strconv"
	"time"
)

type PointData struct {
	DT                time.Time       `json:"dt"`
	IMEI              int64           `json:"imei"`
	PackLat           float64         `json:"pack_lat"`
	PackLon           float64         `json:"pack_lon"`
	Speed             float64         `json:"speed"`
	Course            float32         `json:"course"`
	VDOP              float64         `json:"vdop"`
	HDOP              float64         `json:"hdop"`
	PDOP              float64         `json:"pdop"`
	NSat              int             `json:"nsat"`
	NS                int             `json:"ns"`
	ANSensor          json.RawMessage `json:"an_sensor"`           // Используем RawMessage, т.к. не знаем структуру данных
	LiquidLevelSensor json.RawMessage `json:"liquid_level_sensor"` // То же самое здесь
	Geo               GeoJSON         `json:"geo"`
	ArhivDate         sql.NullTime    `json:"arhiv_date"`
	// ... другие поля, если они есть ...
}

type RawPoints struct {
	ID       int                    `gorm:"primary_key" json:"id"`
	Point    map[string]interface{} `gorm:"size:255" json:"point"`
	Inserted time.Time              `gorm:"<-:false" json:"inserted"`
}

func SaveRawPoint(pointData map[string]interface{}) (err error) {
	point := RawPoints{Point: pointData}
	err = parseAndSaveMTSO(pointData)
	_ = db.DB.Create(&point)

	return nil
}

func ListRawPoints(dtStart, dtFinish string) (points []RawPoints) {
	db.DB.Select("id, point, inserted + INTERVAL '3 hour' as inserted").Where("inserted > (?::timestamp - '3 hour'::interval)", dtStart).Where("inserted <= (?::timestamp - '3 hour'::interval)", dtFinish).Find(&points)
	return
}

func parseAndSaveMTSO(pointData map[string]interface{}) (err error) {
	var (
		imei                    int64
		nsat                    int
		hdop, pdop, vdop, speed float64
		course                  float32
		latitude, longitude     float64
		anSensors               JSONB
		liquidSensors           JSONB
		receivedTime            int64
	)
	imeiStr, ok := pointData["imei"].(string)
	if !ok {
		return fmt.Errorf("imei is not a string or missing")
	}

	imei, err = strconv.ParseInt(imeiStr, 10, 64)
	if err != nil {
		return fmt.Errorf("error parsing imei: %v", err)
	}
	hdop = pointData["hdop"].(float64)
	nsat = int(pointData["nsat"].(float64))
	pdop = pointData["pdop"].(float64)
	vdop = pointData["vdop"].(float64)
	speed = pointData["speed"].(float64)
	course = float32(pointData["course"].(float64))
	latitude = pointData["latitude"].(float64)
	longitude = pointData["longitude"].(float64)
	receivedTime = int64(pointData["navigation_unix_time"].(float64))

	rawAnSensors := pointData["an_sensors"].([]interface{})
	for _, sensor := range rawAnSensors {
		anSensors = append(anSensors, sensor.(map[string]interface{}))
	}

	if pointData["liquid_sensors"] != nil {
		rawLiquidSensors := pointData["liquid_sensors"].([]interface{})
		for _, sensor := range rawLiquidSensors {
			liquidSensors = append(liquidSensors, sensor.(map[string]interface{}))
		}
	}

	newLocation := MTSOLocationEGTS{
		DT:                time.Now(),
		IMEI:              imei,
		PackDT:            time.Unix(receivedTime, 0),
		PackLat:           latitude,
		PackLon:           longitude,
		Speed:             speed,
		Course:            course,
		VDOP:              vdop,
		HDOP:              hdop,
		PDOP:              pdop,
		NSat:              nsat,
		ANSensor:          anSensors,
		LiquidLevelSensor: liquidSensors,
	}

	err = CreateMTSOLocationEGTS(newLocation)
	return err
}
