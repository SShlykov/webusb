package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.com/SShlykov/webusb/lib/db"
	"log"
	"time"
)

type GeoJSON struct {
	Type        string     `json:"type"`
	Coordinates [2]float32 `json:"coordinates"`
}

type sensorsStruct struct {
	SensorNumber int
	Value        int
}

type MTSOLocationEGTS struct {
	ID                int64        `gorm:"primary_key;auto_increment:false" json:"id"`
	DT                time.Time    `gorm:"type:timestamp" json:"dt"`
	IMEI              int64        `json:"imei"`
	PackDT            time.Time    `gorm:"type:timestamp;not null" json:"pack_dt"`
	PackLat           float64      `json:"pack_lat"`
	PackLon           float64      `json:"pack_lon"`
	Speed             float64      `gorm:"type:numeric(5,2)" json:"speed"`
	Course            float32      `json:"course"`
	VDOP              float64      `gorm:"type:numeric(5,2)" json:"vdop"`
	HDOP              float64      `gorm:"type:numeric(5,2)" json:"hdop"`
	PDOP              float64      `gorm:"type:numeric(5,2)" json:"pdop"`
	NSat              int          `json:"nsat"`
	NS                int          `json:"ns"`
	ANSensor          JSONB        `gorm:"type:jsonb;default:'[]'" json:"an_sensor"`           // Используем Jsonb для JSON полей
	LiquidLevelSensor JSONB        `gorm:"type:jsonb;default:'[]'" json:"liquid_level_sensor"` // Используем Jsonb для JSON полей
	Geo               GeoJSON      `gorm:"type:geography(Point,4326)" json:"-"`
	ArhivDate         sql.NullTime `gorm:"type:date" json:"-"`
}

func CreateMTSOLocationEGTS(mtso MTSOLocationEGTS) error {
	geo := fmt.Sprintf("POINT(%f %f)", mtso.PackLat, mtso.PackLon)

	query := `INSERT INTO mtso_locations_egts ( dt, imei, pack_dt, pack_lat, pack_lon, speed, course, vdop, hdop, pdop, nsat, ns, an_sensor, liquid_level_sensor, geo, arhiv_date) 
			  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ST_GeomFromText(?), ?)`

	for i, val := range mtso.ANSensor {
		value, ok := val["value"].(float64)
		if !ok {
			log.Fatalf("Value is not a number")
		}

		sensor, ok := val["sensor_number"].(int)
		if !ok {
			sensor = i + 1
		}

		switch sensor {
		case 1:
			mtso.ANSensor[i] = map[string]interface{}{
				"sensor_number": sensor,
				"value":         value / 1000,
				"desc":          "Напряжение АКБ, мВ",
			}
		case 2:
			mtso.ANSensor[i] = map[string]interface{}{
				"sensor_number": sensor,
				"value":         value / 1000,
				"desc":          "Напряжение внешнего питания, мВ",
			}
		default:
			mtso.ANSensor[i] = map[string]interface{}{
				"sensor_number": sensor,
				"value":         float64(int16(value)) / 100.0,
				"desc":          fmt.Sprintf("Температурный датчик №%v", sensor-2),
			}
		}
	}

	var jsonANSensor []byte
	jsonANSensor, _ = json.Marshal(mtso.ANSensor)

	var jsonLiquidLevelSensor []byte
	jsonLiquidLevelSensor, _ = json.Marshal(mtso.LiquidLevelSensor)

	// Exec выполняет запрос, используя данные из структуры mtso.
	// Важно, чтобы количество плейсхолдеров соответствовало количеству передаваемых параметров.
	err := db.Db177Ent.Exec(query,
		mtso.DT,
		mtso.IMEI,
		mtso.PackDT,
		mtso.PackLat,
		mtso.PackLon,
		mtso.Speed,
		mtso.Course,
		mtso.VDOP,
		mtso.HDOP/10,
		mtso.PDOP,
		mtso.NSat,
		nil,
		jsonANSensor,
		jsonLiquidLevelSensor,
		geo, // здесь передается сформированная строка геометрии
		mtso.ArhivDate,
	).Error

	// Если во время выполнения запроса произошла ошибка, она будет возвращена.
	if err != nil {
		fmt.Println("--------error-------")
		fmt.Println(err)
		fmt.Println("--------error-------")
	}
	return err
}
func GetLastMTSOInfo(imei int64) (mtso MTSOLocationEGTS, err error) {
	query := `select id, dt as dt, imei, pack_dt as pack_dt, pack_lat, pack_lon, speed, course, vdop, hdop, pdop, nsat, ns, an_sensor, liquid_level_sensor from mtso_locations_egts where imei = ? order by pack_dt desc limit 1`

	err = db.Db177Ent.Raw(query, imei).Scan(&mtso).Error
	if err != nil {
		return mtso, err
	}

	return mtso, nil
}

func GetMTSOLocationsBetweenDates(imei int64, dtStart, dtFinish string, locations *[]MTSOLocationEGTS) error {
	query := `SELECT id, dt as dt, imei, pack_dt as pack_dt, 
    				pack_lat, pack_lon, speed, course, vdop, hdop, pdop, nsat, ns, an_sensor, liquid_level_sensor 
			  	FROM mtso_locations_egts 
    			WHERE imei = ? and pack_dt > ?::timestamp - INTERVAL '3 hours' and pack_dt < ?::timestamp - INTERVAL '3 hours'`

	return db.Db177Ent.Raw(query, imei, dtStart, dtFinish).Scan(locations).Error
}

//func UpdateMTSOLocationEGTS(mtso MTSOLocationEGTS) error {
//	geo := fmt.Sprintf("POINT(%f %f)", mtso.Geo.Lng, mtso.Geo.Lat)
//	query := `UPDATE mtso_locations_egts SET dt = ?, imei = ?, pack_dt = ?, pack_lat = ?, pack_lon = ?, speed = ?, course = ?, vdop = ?, hdop = ?, pdop = ?, n_sat = ?, ns = ?, an_sensor = ?, liquid_level_sensor = ?, geo = ST_GeomFromText(?), arhiv_date = ? WHERE id = ?`
//
//	return db.Db177Ent.Exec(query, mtso.DT, mtso.IMEI, mtso.PackDT, mtso.PackLat, mtso.PackLon, mtso.Speed, mtso.Course, mtso.VDOP, mtso.HDOP, mtso.PDOP, mtso.NSat, mtso.NS, mtso.ANSensor, mtso.LiquidLevelSensor, geo, mtso.ArhivDate, mtso.ID).Error
//}

func DeleteMTSOLocationEGTS(id int64) error {
	query := `DELETE FROM mtso_locations_egts WHERE id = ?`
	return db.Db177Ent.Exec(query, id).Error
}
