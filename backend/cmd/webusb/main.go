package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	controllers2 "gitlab.com/SShlykov/webusb/lib/controllers"
	"gitlab.com/SShlykov/webusb/lib/db"
	"gitlab.com/SShlykov/webusb/lib/logs"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "4000"
	}
	db.Connect()
	db.Connect177Db()
	e := echo.New()

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "ok")
	})
	e.POST("/api_communications/save/raw_points", controllers2.GetPoints)
	e.POST("/api/raw_points/list", controllers2.ListPoints)

	e.GET("/api/message", controllers2.GetWebUsb)
	e.POST("/api/get_init_data", controllers2.GetConfig)
	e.POST("/api/save_init_data", controllers2.PostConf)
	e.POST("/api/make_commands", controllers2.PostDevice)

	e.POST("/api/get_last_machine_info", controllers2.GetLastMachineInfo)
	e.POST("/api/get_machine_info", controllers2.GetMTSOLocations)

	e.Use(middleware.CORS())
	logs.InitLog(port)
	e.Logger.Fatal(e.Start(":" + port))
}
