#!/usr/bin/env bash

#xhost +
key="$1"

case $key in
  -f|--front-dev)
    echo "only back started"
    docker compose -f docker-compose.frontdev.yml up  --build
    exit 0
  ;;
  -p|--prod)
    echo "prod started"
    docker compose -f docker-compose.yml up --build -d
    exit 0
  ;;
  -d|--dev)
    echo "dev started"
    docker compose -f docker-compose.yml up --build -d
    exit 0
  ;;
esac

docker compose -f docker-compose.yml up --build